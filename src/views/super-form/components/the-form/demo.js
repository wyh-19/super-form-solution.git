/**
 * @Description:
 * @Author: wyh19
 * @Date: 2021-06-02
 */
export function ajaxGetData() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(
        {
          name: 'wyh',
          age: 30,
          education: 1,
          gender: 1,
          hobby: [1, 3],
          company: 'aaa'
        }
      )
    }, 500)
  })
}

export function ajaxGetOldData() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(
        {
          name: 'wyh19',
          age: 30,
          education: 2,
          gender: 2,
          hobby: [2, 3],
          company: 'bbb'
        }
      )
    }, 1000)
  })
}
